# STEPS

`loadkeys [keyboard-layout]`

`ping archlinux.org`

Failure?
- `./install/connect`

`timedatectl set-ntp true`

## Partitions

`fdisk -l` -- see disks

`fdisk /dev/sdX` -- edit the disk identified by letter X

- `d` to delete a partition
- `n` to create a partition
- `t` to edit a partition type
    - `1` is EFI
    - `19` is Linux Swap
    - `20` is Linux Filesystem

You should choose three partitions I, J and K for EFI boot, Swap and Proper 
filesystem, respectively.

To format partitions use:

- `mkfs.fat -F32 /dev/sdXI`
- `mkswap /dev/sdXJ`
- `swapon /dev/sdXJ`
- `mkfs.ext4 /dev/sdXK`

`mount /mnt /dev/sdXK`

`mkdir /mnt/boot`

`mount /mnt/boot /dev/sdXI`

## Populate The New Filesystem

`./install/packages`

`genfstab -U /mnt >> /mnt/etc/fstab`

`arch-chroot /mnt`

-- TODO --
