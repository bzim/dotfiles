import Control.Monad (forM_, join, guard)
import Control.Exception (tryJust)
import System.IO.Error (isAlreadyExistsError)
import Data.List (sortBy)
import Data.Function (on)
import Data.Maybe (isJust)
import XMonad
  ( def
  , layoutHook
  , terminal
  , logHook
  , modMask
  , normalBorder
  , normalBorderColor
  , focusedBorderColor
  , borderWidth
  , mod4Mask
  , spawn
  , windowset
  , WorkspaceId
  , xK_Print
  , KeySym
  , xmonad
  , gets
  , io
  , X
  )
import XMonad.Util.EZConfig (additionalKeys)
import XMonad.Util.Run (safeSpawn)
import System.Posix.Files (createNamedPipe, stdFileMode)
import XMonad.Hooks.ManageDocks (docks, avoidStruts)
import XMonad.Util.NamedWindows (getName)
import XMonad.StackSet (Workspace, workspaces, currentTag, peek, tag, stack)

workspaceLog :: FilePath
workspaceLog = "/tmp/xmonad-workspace-log"

titleLog :: FilePath
titleLog = "/tmp/xmonad-title-log"

maxTitle :: Int
maxTitle = 37

xf86xK_BrightnessUp :: KeySym
xf86xK_BrightnessUp = 0x1008ff02

xf86xK_BrightnessDown :: KeySym
xf86xK_BrightnessDown = 0x1008ff03

xf86xK_AudioLowerVolume :: KeySym
xf86xK_AudioLowerVolume = 0x1008ff11

xf86xK_AudioMute :: KeySym
xf86xK_AudioMute = 0x1008ff12

xf86xK_AudioRaiseVolume :: KeySym
xf86xK_AudioRaiseVolume = 0x1008ff13

main :: IO ()
main = do
  safeSpawn "picom" []

  tryJust (guard . isAlreadyExistsError) $ do
    createNamedPipe workspaceLog stdFileMode
    createNamedPipe titleLog stdFileMode

  safeSpawn "stalonetray" []
  safeSpawn "launch-polybar" ["xmonad-bar"]

  let myConfig = docks $ def
        { terminal = "kitty"
        , layoutHook = avoidStruts $ layoutHook def
        , logHook = eventLogHook
        , modMask = mod4Mask
        , normalBorderColor = "#1c1c1c"
        , focusedBorderColor = "#ffffff"
        , borderWidth = 1
        }

      myKeys =
        [ ((0, xf86xK_BrightnessDown), spawn "/home/bruno/.local/bin/xk-brightness-down")
        , ((0, xf86xK_BrightnessUp), spawn "/home/bruno/.local/bin/xk-brightness-up")
        , ((0, xf86xK_AudioMute), spawn "/home/bruno/.local/bin/xk-audio-toggle")
        , ((0, xf86xK_AudioLowerVolume), spawn "/home/bruno/.local/bin/xk-audio-down")
        , ((0, xf86xK_AudioRaiseVolume), spawn "/home/bruno/.local/bin/xk-audio-up")
        , ((0, xK_Print), spawn "flameshot gui")
        ]

  xmonad $ additionalKeys myConfig myKeys

formatWorkspace :: WorkspaceId -> WorkspaceId -> String
formatWorkspace currWs ws
  | currWs == ws = "[" ++ ws ++ "]"
  | otherwise    = ws

formatWorkspaces :: WorkspaceId -> [Workspace WorkspaceId l a] -> String
formatWorkspaces currWs =
    (' ' :)
    . (++ " ")
    . join
    . map (formatWorkspace currWs)
    . sortBy compare
    . map tag
    . filter showWs
    where
      showWs ws = currWs == tag ws || isJust (stack ws)

formatTitle :: String -> String -> String
formatTitle workspacesStr title = cut title available
  where
    available = maxTitle - length workspacesStr
    cut []          _ = []
    cut (_ : _ : _) 1 = "…"
    cut (x : xs)    n = x : cut xs (n - 1)

eventLogHook :: X ()
eventLogHook = do
  winset <- gets windowset
  title <- maybe (return "") (fmap show . getName) . peek $ winset
  let wsStr = formatWorkspaces (currentTag winset) (workspaces winset)
  let titleStr = formatTitle wsStr title
  io $ appendFile titleLog (titleStr ++ "\n")
  io $ appendFile workspaceLog (wsStr ++ "\n")
