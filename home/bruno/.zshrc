# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory nomatch notify prompt_subst
unsetopt autocd beep extendedglob
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/bruno/.zshrc'

autoload colors && colors
autoload -Uz compinit
autoload -Uz vcs_info
compinit
# End of lines added by compinstall

# bindkey "^[[1;5C" forward-word
# bindkey "^[[1;5D" backward-word
# bindkey "^H" backward-kill-word


COLOR_RESET="%{$(echo -ne "\e[0m")%}"
COLOR_RED="%{$(echo -ne "\e[31m")%}"
COLOR_GREEN="%{$(echo -ne "\e[32m")%}"
COLOR_YELLOW="%{$(echo -ne "\e[33m")%}"
COLOR_LIGHT_RED="%{$(echo -ne "\e[91m")%}"
COLOR_LIGHT_GREEN="%{$(echo -ne "\e[92m")%}"
COLOR_LIGHT_YELLOW="%{$(echo -ne "\e[93m")%}"
COLOR_LIGHT_BLUE="%{$(echo -ne "\e[94m")%}"
COLOR_LIGHT_CYAN="%{$(echo -ne "\e[96m")%}"
COLOR_LIGHT_MAGENTA="%{$(echo -ne "\e[95m")%}"
COLOR_LIGHT_BLACK="%{$(echo -ne "\e[90m")%}"

VCS_PREFIX="  "
VCS_BRANCH="$COLOR_LIGHT_MAGENTA%b$COLOR_RESET"
VCS_ACTION="[$COLOR_LIGHT_YELLOW%a$COLOR_RESET]"
VCS_UNSTAGED="$COLOR_LIGHT_RED%u$COLOR_RESET"
VCS_STAGED="$COLOR_LIGHT_GREEN%c$COLOR_RESET"

zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' unstagedstr ' *'
zstyle ':vcs_info:*' stagedstr ' +'
zstyle ':vcs_info:git:*' formats \
    "$VCS_PREFIX$VCS_BRANCH$VCS_STAGED$VCS_UNSTAGED"
zstyle ':vcs_info:git:*' actionformats \
    "$VCS_PREFIX$VCS_BRANCH$VCS_ACTION$VCS_STAGED$VCS_UNSTAGED"

precmd() {
    EXIT_CODE=$?

    PROMPT=''
    
    echo -ne "\e]0;$PWD:$HOST\a"
    if [ $EXIT_CODE != 0 ]
    then
        PROMPT="$PROMPT$COLOR_LIGHT_RED$EXIT_CODE"
        PROMPT="$PROMPT$COLOR_RESET "
    fi

    if [ "$PWD" = "$HOME" ]
    then
        DIR="~"
    else
        DIR="$(basename $PWD)"
    fi

    PROMPT="$PROMPT$COLOR_LIGHT_GREEN$USER$COLOR_RESET"
    PROMPT="$PROMPT:$COLOR_LIGHT_CYAN$DIR$COLOR_RESET"
    vcs_info
    VCS="${vcs_info_msg_0_}"
    if git status 2> /dev/null > /dev/null
    then
        AHEAD=''
        BEHIND=''
        if git status -sb | grep ahead 2> /dev/null > /dev/null
        then
            AHEAD=$(git status -sb \
                | grep -o 'ahead [[:digit:]]\+' \
                | grep -o '[[:digit:]]\+')
            AHEAD=" $COLOR_YELLOW↑$AHEAD$COLOR_RESET"
        fi
        if git status -sb | grep behind 2> /dev/null > /dev/null
        then
            BEHIND=$(git status -sb \
                | grep -o 'behind [[:digit:]]\+' \
                | grep -o '[[:digit:]]\+')
            BEHIND=" $COLOR_LIGHT_CYAN↓$BEHIND$COLOR_RESET"
        fi
        VCS="$VCS$AHEAD$BEHIND"
    fi
    PROMPT="$PROMPT$VCS$COLOR_LIGHT_BLACK%%$COLOR_RESET "
}

set -o vi

zle-line-init () {
    zle -K vicmd
}

zle -N zle-line-init

bindkey -M viins çç vi-cmd-mode
bindkey -M viopp çç vi-cmd-mode
bindkey -M visual çç vi-cmd-mode
