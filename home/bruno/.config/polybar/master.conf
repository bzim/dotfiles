;=====================================================
;     Colors & Settings
;=====================================================

[colors]
background = ${xrdb:background:#80000000}
foreground = ${xrdb:foreground:#dde0e7}
moderate = ${xrdb:color5:#ebcb8b}
urgent = ${xrdb:color3:#bf616a}
trim = ${xrdb:color6:#919ba0}
blue = ${xrdb:color1:#a2d1ec}
purple = ${xrdb:color2:#b48ead}
border = ${xrdb:background:#80000000}


[settings]
throttle-output = 5
throttle-output-for = 10
throttle-input-for = 30
compositing-background = source
compositing-foreground = source
compositing-overline = source
compositing-underline = source
compositing-border = source
screenchange-reload = true
format-foreground = ${colors.foreground}
format-background = ${colors.background}

;=====================================================
;    Master Bar
;=====================================================

[bar/master]
width = 100%
height = 27
border-size = 0
border-top-size = 0
border-bottom-size = 1
radius = 0
bottom = false
line-size = 3

monitor = eDP1

font-0 = "Fira Code:size=11;2"
font-1 = "Noto Sans:style=Regular:size=11;2"
font-2 = "DejaVu Sans:size=11;2"
font-3 = "NotoEmoji:scale=11:size=11;1.5"
font-4 = "Fira Code:style=Bold:size=10;2"

offset-x = 0
offset-y = 0
fixed-center = true
border-color = ${colors.border}
background = ${colors.background}
foreground = ${colors.foreground}
module-margin = 0.5
padding-right = 0
padding-left = 0
separator =

; shows which modules have click actions
cursor-click = pointer

; with cursor-click = pointer' these cause the entire bar to have click actions
; essentially defeating the purpose, so they are commented out

; scroll-up = pamixer -i 2
; scroll-down = pamixer -d 2
; click-middle = skippy-xd --activate-window-picker
; double-click-left = networkmanager_dmenu
; double-click-middle = skippy-xd --activate-window-picker
; double-click-right = pavucontrol &


; Position of the system tray window
; If empty or undefined, tray support will be disabled
; NOTE: A center aligned tray will cover center aligned modules
;
; Available positions:
;   left
;   center
;   right
;   none
tray-position = right

; If true, the bar will not shift its
; contents when the tray changes
tray-detached = false

; Tray icon max size
tray-maxsize = 16

; Background color for the tray container
; ARGB color (e.g. #f00, #ff992a, #ddff1023)
; By default the tray container will use the bar
; background color.
; tray-background = ${root.background}

; Tray offset defined as pixel value (e.g. 35) or percentage (e.g. 50%)
tray-offset-x = 0
tray-offset-y = 0

; Pad the sides of each tray icon
tray-padding = 1

; Scale factor for tray clients
tray-scale = 1.0
