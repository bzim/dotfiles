vim.opt.runtimepath:prepend('~/.vim')
vim.opt.runtimepath:append('~/.vim/after')
vim.opt.packpath = vim.opt.runtimepath:get()
vim.opt.termguicolors = true

local vim = vim
local Plug = vim.fn['plug#']

vim.call('plug#begin')

Plug('dracula/vim')
Plug('nvim-tree/nvim-web-devicons')
Plug('nvim-tree/nvim-tree.lua')
Plug('nvim-lua/plenary.nvim')
Plug('nvim-telescope/telescope.nvim', {  tag = '0.1.8' })
Plug('neovim/nvim-lspconfig')
Plug('hrsh7th/cmp-nvim-lsp')
Plug('hrsh7th/cmp-buffer')
Plug('hrsh7th/cmp-path')
Plug('hrsh7th/cmp-cmdline')
Plug('hrsh7th/nvim-cmp')
Plug('lukas-reineke/lsp-format.nvim')
Plug('MunifTanjim/prettier.nvim')
Plug('https://github.com/elixir-editors/vim-elixir')
Plug('elixir-tools/elixir-tools.nvim', { tag = 'stable' })
Plug('nvim-pack/nvim-spectre')
Plug('rmagatti/auto-session')

vim.call('plug#end')

vim.cmd('source ~/.vimrc')

vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

local nvim_tree = require('nvim-tree')
nvim_tree.setup({
    view = { relativenumber = true },
    actions = {
        expand_all = {
            max_folder_discovery = 100,
            exclude = { '.git' }
        },
    },
    update_focused_file = {
        enable = true,
    },
})

local spectre = require('spectre')

vim.keymap.set('n', '<Space><Space>', ':NvimTreeToggle<cr>')
vim.keymap.set('n', '<Space><S-f>', ':NvimTreeFocus<cr>')
vim.keymap.set('n', '<Space>s', function()
    spectre.toggle()
end)
vim.keymap.set('n', 'ft', ':Telescope<cr>')
vim.keymap.set('n', 'ff', ':Telescope find_files<cr>')
vim.keymap.set('n', 'fe', ':Telescope diagnostics severity=error<cr>')
vim.keymap.set('n', 'fw', ':Telescope diagnostics severity=warn<cr>')
vim.keymap.set('n', 'fd', ':Telescope diagnostics<cr>')
vim.keymap.set('n', 'fb', ':Telescope buffers<cr>')
vim.keymap.set('n', 'fr', ':Telescope resume<cr>')
vim.keymap.set('n', 'fi', ':Telescope lsp_implementations<cr>')
vim.keymap.set('n', 'f<S-d>', ':Telescope lsp_definitions<cr>')
vim.keymap.set('n', 'f<S-t>', ':Telescope lsp_type_definitions<cr>')

local lspconfig = require('lspconfig')
local cmp = require('cmp')
local lsp_format = require("lsp-format")

cmp.setup({
    sources = cmp.config.sources({
        { name = 'nvim_lsp' }
    }, {
        { name = 'buffer' },
    }),
    mapping = cmp.mapping.preset.insert({
        ['<CR>'] = cmp.mapping.confirm({
            select = true,
            behavior = cmp.ConfirmBehavior.Insert,

         }),
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<C-j>'] = cmp.mapping.select_next_item({
            behavior = cmp.SelectBehavior.Select,
        }),
        ['<C-k>'] = cmp.mapping.select_prev_item({
            behavior = cmp.SelectBehavior.Select,
        }),
        ['<C-n>'] = cmp.mapping.select_next_item({
            behavior = cmp.SelectBehavior.Select,
        }),
        ['<C-p>'] = cmp.mapping.select_prev_item({
            behavior = cmp.SelectBehavior.Select,
        }),
        ['<Esc>'] = cmp.mapping.abort(),
    }),
})

local capabilities = require('cmp_nvim_lsp').default_capabilities()

lsp_format.setup()

vim.lsp.handlers['textDocument/publishDiagnostics'] = vim.lsp.with(
    vim.lsp.diagnostic.on_publish_diagnostics, {
        -- virtual_text = false,
    }
)

vim.diagnostic.config({
    signs = false,
    float = {
        show_header = true,
        source = 'always',
        border = 'rounded',
        focusable = true,
    },
    underline = true,
    update_in_insert = false,
})

local global_on_attach = function(args)
    lsp_format.on_attach(args)
end

local default_lsp_handlers = {}
for method, default_handler in pairs(vim.lsp.handlers) do
    default_lsp_handlers[method] = default_handler
end

for method, default_handler in pairs(default_lsp_handlers) do
    vim.lsp.handlers[method] = function(err, result, context, config)
        if err ~= nil and (err.code == -32802 or err.code == -32603) then
            return
        end
        return default_handler(err, result, context, config)
    end
end

lspconfig.rust_analyzer.setup({
    on_attach = global_on_attach,
    settings = {
        ['rust-analyzer'] = {
            cargo = {
                allFeatures = true,
                buildScripts = {
                    enable = true,
                },
            },
            procMacro = {
                enable = true,
            },
        },
    },
    capabilities = capabilities,
})

lspconfig.ts_ls.setup({
    on_attach = global_on_attach,
    capabilities = capabilities,
})

vim.keymap.set('n', '<Space>e', function()
    vim.diagnostic.open_float({
        noremap = true,
        silent = true, 
        buffer = bufnr,
    })
end)

vim.keymap.set('n', '<Space>2', function()
    vim.lsp.buf.rename()
end)

vim.keymap.set('n', '<Space>f', function()
    vim.lsp.buf.code_action({
        context = {
            only = { 'quickfix' },
        },
    })
end)

require('telescope').setup({
    defaults = {
        mappings = {
            i = {
                ['ÇÇ'] = 'close',
            },
            n = {
                ['çç'] = 'close',
            },
        },
    },
})

require('elixir').setup({
    nextls = { enable = false },
    elixirls = { enable = true },
    projectionist = { enable = true },
})

vim.o.sessionoptions="blank,buffers,curdir,folds,help,tabpages,winsize,winpos,terminal,localoptions"

require('auto-session').setup({
    suppressed_dirs = { '~/', '~/Documents', '~/Downloads' },
})
