"colorscheme dracula
if has('nvim')
    hi Normal guibg=NONE
    hi TabLine guibg=#282828 guifg=#909090 gui=bold
    hi TabLineSel guibg=#9f9f9f guifg=#000000 gui=bold
    hi TabLineFill guibg=NONE guifg=#a8a8a8 gui=bold
    hi LineNr guibg=#404040 guifg=#e0a2ff
    hi! link CursorLineNr LineNr
    hi LineSurround guibg=#282828 guifg=#d098ef
    hi link LineNrAbove LineSurround
    hi link LineNrBelow LineSurround
    hi Comment guifg=#b09dd8
    hi SpecialComment guifg=#b8a8e0
    hi link rustCommentLineDoc SpecialComment
    hi @lsp.type.typeParameter guifg=#ffffff
    hi DraculaCyan gui=None guifg=#b8f0ff
    hi! link DraculaCyanItalic DraculaCyan
    hi! link DraculaOrangeItalic DraculaOrange
    hi Constant guifg=#d898ff
    hi! DraculaPink guifg=#ffa8d0
    hi! DraculaOrange guifg=#ffd8a8
    hi! DraculaGreen guifg=#98ffaf
    hi! link DraculaGreenItalic DraculaGreen
else
    hi Normal ctermbg=0
    hi TabLine ctermbg=0 ctermfg=250 cterm=bold
    hi TabLineFill ctermbg=0 ctermfg=250 cterm=bold
    hi LineNrAbove ctermbg=234 ctermfg=140
    hi LineNr ctermbg=237 ctermfg=183
    hi LineNrBelow ctermbg=234 ctermfg=140
    hi TabLineSel ctermbg=241 ctermfg=255 cterm=bold
    hi Comment ctermfg=140
    hi SpecialComment ctermfg=183
    hi link rustCommentLineDoc SpecialComment
    hi Type cterm=None ctermfg=123
    hi Constant cterm=bold ctermfg=141

    hi Pmenu ctermfg=254 ctermbg=235
    hi PmenuSel ctermfg=254 ctermbg=238

    hi CocFloating ctermfg=254 ctermbg=235
    hi CocMenuSel ctermfg=254 ctermbg=238

    hi CocErrorLine ctermbg=1
    hi CocErrorSign ctermbg=52 guibg=#ff5f5f ctermfg=203 guifg=#870000
    hi CocErrorVirtualText ctermfg=88 guifg=#870000
    hi CocErrorFloat ctermbg=52 ctermfg=255 guifg=#870000
    hi FgCocErrorFloatBgCocFloating ctermbg=52 ctermfg=255
    hi CocErrorHighlight cterm=bold ctermbg=52 guibg=#5f0000

    hi CocWarningSign ctermbg=228 guibg=#ffff87 ctermfg=58 guifg=#5f5f00
    hi CocWarningVirtualText ctermfg=58 guifg=#5f5f00
    hi CocWarningFloat ctermbg=58 ctermfg=255
    hi FgCocWarningFloatBgCocFloating ctermbg=58 ctermfg=255
    hi CocWarningHighlight cterm=bold ctermbg=58 guibg=#5f5f00

    hi CocUnusedHighlight cterm=bold ctermbg=58 guibg=#5f5f00
endif
