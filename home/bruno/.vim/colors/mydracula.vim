hi colorcolumn ctermbg=238

hi Visual term=reverse ctermbg=242 guibg=DarkGrey

let s:colors = [249, 215, 185, 114, 86, 75, 176, 219]

exec 'hi Comment ctermfg=' . s:colors[0]
exec 'hi Constant ctermfg=' . s:colors[1]
exec 'hi LineNr ctermfg=' . s:colors[1]
exec 'hi String ctermfg=' . s:colors[2]
exec 'hi Identifier ctermfg=' . s:colors[3]
exec 'hi Type ctermfg=' . s:colors[4]
exec 'hi PreProc ctermfg=' . s:colors[5]
exec 'hi Statement ctermfg=' . s:colors[6]
exec 'hi Special ctermfg=' . s:colors[7]
exec 'hi SpecialComment cterm=bold ctermfg=' . s:colors[0]
