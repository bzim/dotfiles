"colorscheme habamax
if has('nvim')
    hi Normal ctermbg=0 guibg=0
else
    hi Normal ctermbg=0
endif

hi LineNrAbove ctermbg=233 ctermfg=131
hi LineNr ctermbg=236 ctermfg=174
hi LineNrBelow ctermbg=233 ctermfg=131

hi Pmenu ctermfg=254 ctermbg=235
hi PmenuSel ctermfg=254 ctermbg=238

if !has('nvim')
    hi CocFloating ctermfg=254 ctermbg=235
    hi CocMenuSel ctermfg=254 ctermbg=238

    hi CocErrorLine ctermbg=1
    hi CocErrorSign ctermbg=203 guibg=#ff5f5f ctermfg=88 guifg=#870000
    hi CocErrorVirtualText ctermfg=88 guifg=#870000
    hi CocErrorFloat ctermbg=52 ctermfg=255 guifg=#870000
    hi FgCocErrorFloatBgCocFloating ctermbg=52 ctermfg=255
    hi CocErrorHighlight cterm=bold ctermbg=52 guibg=#5f0000

    hi CocWarningSign ctermbg=228 guibg=#ffff87 ctermfg=58 guifg=#5f5f00
    hi CocWarningVirtualText ctermfg=58 guifg=#5f5f00
    hi CocWarningFloat ctermbg=58 ctermfg=255
    hi FgCocWarningFloatBgCocFloating ctermbg=58 ctermfg=255
    hi CocWarningHighlight cterm=bold ctermbg=58 guibg=#5f5f00

    hi CocUnusedHighlight cterm=bold ctermbg=58 guibg=#5f5f00
endif
