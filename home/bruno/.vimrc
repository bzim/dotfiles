if !has('nvim')
    call plug#begin('~/.vim/vim-plug')

    Plug 'elixir-editors/vim-elixir'
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'cespare/vim-toml'

    Plug 'dracula/vim'

    call plug#end()
endif

syntax on
filetype plugin indent on

colorscheme dracula
colorscheme dracula_ext

"colorscheme dracula_ext

set tabstop=4
set shiftwidth=4
set expandtab
set relativenumber

if has('nvim')
    "make vim clipboard the same as the system one
    set clipboard^=unnamed,unnamedplus
else
    set clipboard=unnamedplus   "make vim clipboard the same as the system one
    set clipboard^=autoselect   "copy on selection like in X
endif

set number
set signcolumn=number
set colorcolumn=81

autocmd FileType reason setlocal shiftwidth=2 softtabstop=2 expandtab
autocmd FileType yaml setlocal shiftwidth=2 softtabstop=2 expandtab
autocmd FileType haskell setlocal shiftwidth=2 softtabstop=2 expandtab
autocmd FileType json setlocal shiftwidth=2 softtabstop=2 expandtab
autocmd FileType elixir setlocal shiftwidth=2 softtabstop=2 expandtab
autocmd FileType dart setlocal shiftwidth=2 softtabstop=2 expandtab
autocmd BufRead,BufNewFile *.nasm set filetype=nasm

autocmd FileType reason autocmd BufWritePost <buffer> silent 
    \exec "!refmt --in-place" . %

set backupcopy=yes

set undofile
if has('nvim')
    set undodir=~/.local/share/nvim/undodir
else
    set undodir=~/.vim/undodir
end

inoremap çç <Esc>
snoremap çç <Esc>
xnoremap çç <Esc>
nnoremap çç <Esc>
onoremap çç <Esc>
cnoremap çç <C-C>
vnoremap çç <Esc>
snoremap çç <Esc>
lnoremap çç <Esc>
tnoremap çç <Esc>

nnoremap ça <Home>
nnoremap çs <End>
nnoremap ç<S-a> <C-Home>
nnoremap ç<S-s> <C-End>

inoremap <C-u> <nop>

"CoC config
if !has('nvim')
    command! -nargs=0 Format :call CocAction('format')

    nnoremap <silent><nowait> <space>a  :<C-u>CocList --normal diagnostics<cr>
    nmap <silent> <space>f <Plug>(coc-fix-current)
    nmap <silent> <space>d <Plug>(coc-definition)
    nmap <silent> <space>t <Plug>(coc-type-definition)
    nmap <silent> <space>i <Plug>(coc-implementation)
    nmap <silent> <space>r <Plug>(coc-references)
    nmap <silent> <space>p <Plug>(coc-diagnostic-prev)
    nmap <silent> <space>n <Plug>(coc-diagnostic-next)
    nmap <silent> <space>2 <Plug>(coc-rename)
    nmap <silent> <space>h :CocCommand document.toggleInlayHint<cr>

    " Use tab for trigger completion with characters ahead and navigate
    inoremap <silent><expr> <TAB>
          \ coc#pum#visible() ? coc#pum#next(1) :
          \ CheckBackspace() ? "\<Tab>" :
          \ coc#refresh()
    inoremap <silent><expr><C-Space> coc#refresh()
    inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

    " Make <CR> to accept selected completion item or notify coc.nvim to format
    " <C-g>u breaks current undo, please make your own choice
    inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                                  \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

    function! CheckBackspace() abort
      let col = col('.') - 1
      return !col || getline('.')[col - 1]  =~# '\s'
    endfunction

    set cmdheight=2
endif

"function! DirEdit()
"    let expanded = expand("%:h")
"    if len(expanded) > 0
"        let expanded = expanded . "/"
"    endif
"    let path = input("Open: ", expanded, "file")
"    if len(path) > 0
"        exec ":edit " . path
"    endif
"endfunction
"
":command! DirEdit :call DirEdit()
":command! DE :call DirEdit()
":command! D :call DirEdit()

function! WriteParent ()
    let expanded = expand('%:h')
    exec ':silent !mkdir -p ' . shellescape(expanded)
    exec ':redraw!'
    exec ':write'
endfunction

:command! W :call WriteParent()
:command! WP :call WriteParent()

if !has('nvim')
    function! ExploreWithLast (cmd)
        let l:file = expand('%:t')
        let l:parent = expand('%:h')
        exec a:cmd . ' ' . l:parent
        call search('\V' . l:file, 'w')
    endfunction

    let g:netrw_keepdir = 1
    let g:netrw_localcopydircmd = 'cp -r'
    let g:netrw_liststyle = 3
    let g:netrw_altfile = 1

    nmap <silent> <space><space> :call ExploreWithLast(':Explore')<cr>
    nmap <silent> <space><S-h> :call ExploreWithLast(':Hexplore')<cr>
    nmap <silent> <space><S-v> :call ExploreWithLast(':Vexplore')<cr>
    nmap <silent> <space><S-s> :call ExploreWithLast(':Sexplore')<cr>
    nmap <silent> <space><S-t> :call ExploreWithLast(':Texplore')<cr>
    nmap <silent> <space><S-l> :call ExploreWithLast(':Lexplore')<cr>
    nmap <silent> <space><S-t> :call ExploreWithLast(':Texplore')<cr>
endif


nmap <silent> <space>j :tabprev<cr>
nmap <silent> <space>k :tabnext<cr>
nmap <silent> <space>c :tabnew<cr>
nmap <silent> gk :new<cr>
nmap <silent> gj :botright new<cr>
nmap <silent> gh :vnew<cr>
nmap <silent> gl :botright vnew<cr>
